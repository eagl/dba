package pl.edu.pk.mech.repository;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;
import pl.edu.pk.mech.domain.Query;
import pl.edu.pk.mech.domain.Role;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Repository
public class QueryRepositoryImpl implements QueryRepository
{
    @Resource(name = "queryList")
    private List<Query> queryList;

    @PostConstruct
    public void initialize() throws Exception {
        queryList.forEach((Query query)-> query.setHash(String.valueOf(query.hashCode())));
    }

    @Cacheable("queryList")
    @Override
    public List<Query> getQueryListByRole(Role role)
    {
        Objects.requireNonNull(role);
        return queryList.stream()
                .filter(query -> query.getRoles().contains(role))
                .collect(Collectors.toList());
    }

    @Cacheable("query")
    @Override
    public Query getQueryByHash(String hash)
    {
        Objects.requireNonNull(hash);
        return queryList.stream()
                .filter(query -> query.getHash().equals(hash))
                .findFirst().orElseThrow(IllegalArgumentException::new);
    }

    @Cacheable("queryName")
    @Override
    public Query getQueryByName(String name)
    {
        Objects.requireNonNull(name);
        return queryList.stream()
                .filter(query -> query.getName().equals(name))
                .findFirst().orElseThrow(IllegalArgumentException::new);
    }
}

