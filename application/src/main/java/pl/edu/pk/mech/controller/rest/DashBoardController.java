package pl.edu.pk.mech.controller.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import pl.edu.pk.mech.domain.Query;
import pl.edu.pk.mech.domain.Role;
import pl.edu.pk.mech.dto.QueryDto;
import pl.edu.pk.mech.mapper.Mapper;
import pl.edu.pk.mech.repository.QueryRepository;
import pl.edu.pk.mech.util.QueryUtil;
import pl.edu.pk.mech.util.SessionUtil;

import javax.annotation.Resource;
import java.util.List;

@Controller
public class DashBoardController extends RestController
{
    @Autowired
    private SessionUtil sessionUtil;

    @Autowired
    private QueryRepository queryRepository;

    @Autowired
    private QueryUtil queryUtil;

    @Resource(name = "queryToQueryDtoMapper")
    private Mapper<List<Query>,List<QueryDto>> mapper;

    @ResponseBody
    @RequestMapping("/navigation-query-list")
    public List<QueryDto> getNavigationQueryList()
    {
        Role role = sessionUtil.getRole();
        List<Query> queryList = queryRepository.getQueryListByRole(role);
        queryList = queryUtil.filterByDisplayInNav(queryList);

        return mapper.map(queryList);
    }

    @ResponseBody
    @RequestMapping("/dashboard-query-list")
    public List<QueryDto> getDashboardQueryList()
    {
        Role role = sessionUtil.getRole();
        List<Query> queryList = queryRepository.getQueryListByRole(role);
        queryList = queryUtil.filterByDisplayOnDashboard(queryList);

        return mapper.map(queryList);
    }
}
