package pl.edu.pk.mech.mapper;

import org.springframework.stereotype.Component;
import pl.edu.pk.mech.dto.IResultDto;
import pl.edu.pk.mech.dto.QueryPlotDto;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component(value = "queryToPlotDtoMapper")
public class QueryToPlotDtoMapper implements Mapper<List<Map<String, Object>>, IResultDto>
{
    private static final String USED = "USED";
    private static final String MAX = "MAX";
    private static final String FREE = "FREE";

    @Override
    public IResultDto map(List<Map<String, Object>> list)
    {
        QueryPlotDto plotDto = new QueryPlotDto();
        Map<String, Object> data = new HashMap<>(1);
        data.put(USED, list.get(0).get(USED));
        data.put(FREE, list.get(0).get(FREE));

        plotDto.setData(data);
        plotDto.setMax("MAX USAGE: "+list.get(0).get(MAX)+" MB");

        if (list.get(0).get("STATUS") != null && list.get(0).get("FILE_NAME") != null)
            plotDto.setDescription(list.get(0).get("STATUS")+" - "+list.get(0).get("FILE_NAME"));

        return plotDto;
    }
}
