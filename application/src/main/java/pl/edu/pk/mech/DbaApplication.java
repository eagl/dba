package pl.edu.pk.mech;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.embedded.FilterRegistrationBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CachingConfigurer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.concurrent.ConcurrentMapCache;
import org.springframework.cache.interceptor.KeyGenerator;
import org.springframework.cache.interceptor.SimpleKeyGenerator;
import org.springframework.cache.support.SimpleCacheManager;
import org.springframework.context.ApplicationContext;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.*;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import pl.edu.pk.mech.filter.DataBaseSetterFilter;
import pl.edu.pk.mech.initializer.Initializer;

import java.util.Arrays;
import java.util.HashSet;

@ComponentScan
@Configuration
@EnableAutoConfiguration
@EnableConfigurationProperties
@EnableCaching
@ImportResource({ "classpath:/db.xml", "classpath:/queries.xml" })
public class DbaApplication extends WebMvcConfigurerAdapter implements CachingConfigurer
{
    public static void main(String[] args) throws Exception
    {
        ApplicationContext context = new SpringApplicationBuilder(DbaApplication.class).run(args);

        Initializer jdbcTemplatesToCacheInitializer = (Initializer) context.getBean("jdbcTemplatesToCacheInitializer");
        jdbcTemplatesToCacheInitializer.initialize(context);
    }

    @Override
    public void addViewControllers(ViewControllerRegistry registry)
    {
        registry.addViewController("/login").setViewName("login");
    }

//    @Bean
//    public FilterRegistrationBean contextFilterRegistrationBean()
//    {
//        FilterRegistrationBean registrationBean = new FilterRegistrationBean();
//        registrationBean.setFilter(new DataBaseSetterFilter());
//        registrationBean.setOrder(1);
//        return registrationBean;
//    }

    @Bean
    public ApplicationSecurity applicationSecurity()
    {
        return new ApplicationSecurity();
    }

    @Bean
    public ApplicationCache applicationCache()
    {
        return new ApplicationCache();
    }

    @Bean
    public MessageSource messageSource() {
        ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
        messageSource.setBasename("classpath:/messages/messages");
        return messageSource;
    }

    @Bean
    @Override
    public CacheManager cacheManager()
    {
        ConcurrentMapCache queryCache = new ConcurrentMapCache("query");
        ConcurrentMapCache queryNameCache = new ConcurrentMapCache("queryName");
        ConcurrentMapCache queryListCache = new ConcurrentMapCache("queryList");
        SimpleCacheManager cacheManager = new SimpleCacheManager();
        cacheManager.setCaches(new HashSet<Cache>(Arrays.asList(queryCache, queryNameCache, queryListCache)));
        return cacheManager;
    }

    @Bean
    @Override
    public KeyGenerator keyGenerator()
    {
        return new SimpleKeyGenerator();
    }
}
