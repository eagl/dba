package pl.edu.pk.mech.controller.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import pl.edu.pk.mech.ApplicationCache;
import pl.edu.pk.mech.JdbcTemplatesWrapper;
import pl.edu.pk.mech.dto.DatabaseDto;
import pl.edu.pk.mech.dto.ResultDto;
import pl.edu.pk.mech.util.SessionUtil;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static pl.edu.pk.mech.dto.ResultDto.Result.SUCCESS;

@Controller
public class DatabasesController extends RestController
{
    private static final String STATUS_SQL = "SELECT 1 FROM DUAL";

    @Autowired
    private ApplicationCache applicationCache;

    @Autowired
    private SessionUtil sessionUtil;

    @ResponseBody
    @RequestMapping("/available-databases")
    public List<String> getAvailableDatabases()
    {
        List<String> databases = new ArrayList<>(applicationCache.getDatabaseToJdbcTemplatesWrapperMap().size());
        for(String database : applicationCache.getDatabaseToJdbcTemplatesWrapperMap().keySet())
        {
            databases.add(database);
        }

        return databases;
    }

    @ResponseBody
    @RequestMapping("/databases-status")
    public List<DatabaseDto> getDatabasesStatus()
    {
        List results = new LinkedList<>();

        for (Map.Entry<String, JdbcTemplatesWrapper> entry : applicationCache.getDatabaseToJdbcTemplatesWrapperMap().entrySet())
        {
            String database = entry.getKey();
            JdbcTemplate jdbcTemplate = entry.getValue().getJdbcTemplateForUser();
            try
            {
                jdbcTemplate.queryForObject(STATUS_SQL, new Object[]{}, Integer.class);
                results.add(new DatabaseDto(database, true));
            }
            catch (Exception e)
            {
                results.add(new DatabaseDto(database, false));
            }
        }

        return results;
    }

    @ResponseBody
    @RequestMapping(value = "/database", method = RequestMethod.POST)
    public ResultDto setUsername(@RequestBody DatabaseDto databaseDto)
    {
        String database = databaseDto.getName();
        sessionUtil.setDatabase(database);
        return new ResultDto(SUCCESS);
    }
}
