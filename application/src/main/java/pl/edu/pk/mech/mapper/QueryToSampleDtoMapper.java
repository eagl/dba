package pl.edu.pk.mech.mapper;

import org.springframework.stereotype.Component;
import pl.edu.pk.mech.dto.IResultDto;
import pl.edu.pk.mech.dto.QuerySimpleDto;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component(value = "queryToSampleDtoMapper")
public class QueryToSampleDtoMapper implements Mapper<List<Map<String, Object>>, IResultDto>
{
    @Override
    public IResultDto map(List<Map<String, Object>> list)
    {
        QuerySimpleDto querySimpleDto = new QuerySimpleDto();
        Map<String, Object> data = new HashMap<>(1);
        data.put("value", list.get(0).get("OUTSCORE"));
        querySimpleDto.setData(data);

        return querySimpleDto;
    }
}
