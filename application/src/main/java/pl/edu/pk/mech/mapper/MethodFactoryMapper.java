package pl.edu.pk.mech.mapper;

import org.springframework.stereotype.Component;
import pl.edu.pk.mech.domain.Query;
import pl.edu.pk.mech.dto.IResultDto;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Component(value = "methodFactoryMapper")
public final class MethodFactoryMapper {

    @Resource(name = "queryToGridDtoMapper")
    private Mapper<List<Map<String, Object>>, IResultDto> gridMapper;

    @Resource(name = "queryToSampleDtoMapper")
    private Mapper<List<Map<String, Object>>, IResultDto> sampleMapper;

    @Resource(name = "queryToPlotDtoMapper")
    private Mapper<List<Map<String, Object>>, IResultDto> plotMapper;

    public IResultDto mapResult(Query.Type type, List<Map<String, Object>> rows){
        switch (type){
            case GRID:
                return gridMapper.map(rows);
            case ONE_VALUE:
                return sampleMapper.map(rows);
            case PLOT:
                return plotMapper.map(rows);
        }

        throw new IllegalArgumentException("Element of "+type.getClass().getName()+" do not match to switch syntax");
    }
}
