package pl.edu.pk.mech.initializer;

import org.springframework.context.ApplicationContext;

public interface Initializer
{
    void initialize(ApplicationContext context);
}
