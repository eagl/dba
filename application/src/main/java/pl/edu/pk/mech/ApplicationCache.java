package pl.edu.pk.mech;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class ApplicationCache implements IApplicationCache {
    private Map<String, Object> parameters;
    private Map<String, JdbcTemplatesWrapper> databaseToJdbcTemplatesWrapperMap;

    @Autowired
    private MessageSource messageSource;

    public ApplicationCache() {
        this.parameters = new HashMap<>();
    }

    public String getMessage(String key){
        return messageSource.getMessage(key, null, new Locale("en"));
    }

    @Override
    public Object getParameter(String name) {
        return this.parameters.get(name);
    }

    @Override
    public void addParameter(String key, Object value) {
        this.parameters.put(key, value);
    }

    public Map<String, JdbcTemplatesWrapper> getDatabaseToJdbcTemplatesWrapperMap() {
        return databaseToJdbcTemplatesWrapperMap;
    }

    public void setDatabaseToJdbcTemplatesWrapperMap(Map<String, JdbcTemplatesWrapper> databaseToJdbcTemplatesWrapperMap) {
        this.databaseToJdbcTemplatesWrapperMap = databaseToJdbcTemplatesWrapperMap;
    }
}
