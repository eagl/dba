package pl.edu.pk.mech.dto;

import java.util.Map;

public class QuerySimpleDto extends QueryResponse
{
    private Map<String, Object> data;

    public Map<String, Object> getData() {
        return data;
    }

    public void setData(Map<String, Object> data) {
        this.data = data;
    }
}
