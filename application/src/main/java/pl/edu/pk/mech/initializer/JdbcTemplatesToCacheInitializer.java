package pl.edu.pk.mech.initializer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import pl.edu.pk.mech.ApplicationCache;
import pl.edu.pk.mech.JdbcTemplatesWrapper;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@PropertySource("classpath:/application.properties")
@Component(value = "jdbcTemplatesToCacheInitializer")
public class JdbcTemplatesToCacheInitializer implements Initializer
{
    private static final String BEAN_PREFIX = "oracle.jdbcTemplate%s.";

    @Autowired
    private Environment environment;

    @Autowired
    private ApplicationCache applicationCache;

    @Override
    public void initialize(ApplicationContext context)
    {
        Map<String, JdbcTemplatesWrapper> map = new HashMap<>();

        int datasourceSize = Integer.parseInt(environment.getProperty("oracle.datasource.size"));

        for (int i = 1; i <= datasourceSize; i++)
        {
            String beanPrefix = String.format(BEAN_PREFIX, i);
            String database = environment.getProperty(String.format("oracle.datasource%s.name", i));
            JdbcTemplate jdbcTemplateForAdmin = getJdbcTemplate(beanPrefix + "admin", context);
            JdbcTemplate jdbcTemplateForUser = getJdbcTemplate(beanPrefix + "user", context);
            JdbcTemplatesWrapper wrapper = new JdbcTemplatesWrapper(jdbcTemplateForAdmin, jdbcTemplateForUser);

            map.put(database, wrapper);
        }

        applicationCache.setDatabaseToJdbcTemplatesWrapperMap(Collections.unmodifiableMap(map));
    }

    private JdbcTemplate getJdbcTemplate(String name, ApplicationContext context)
    {
        return (JdbcTemplate) context.getBean(name);
    }
}
