package pl.edu.pk.mech.dto;

public abstract class QueryResponse implements IResultDto {
    public String messageError;

    public String getMessageError() {
        return messageError;
    }

    public void setMessageError(String messageError) {
        this.messageError = messageError;
    }
}
