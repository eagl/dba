package pl.edu.pk.mech.filter;

import org.apache.catalina.connector.RequestFacade;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import pl.edu.pk.mech.UserSessionContext;
import javax.servlet.*;
import java.io.IOException;

@Deprecated
public class DataBaseSetterFilter implements Filter
{
    @Override
    public void init(FilterConfig filterConfig) throws ServletException
    {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) throws IOException, ServletException
    {
        String requestedURI = ((RequestFacade) request).getRequestURI();
        if (requestedURI.equals("/login") || requestedURI.equals("/login-limited-user"))
        {
            if (request.getParameter("database") != null)
            {
                WebApplicationContext applicationContext = WebApplicationContextUtils.getWebApplicationContext(request.getServletContext());
                UserSessionContext sessionContext = (UserSessionContext) applicationContext.getBean("userSessionContext");
                String database = request.getParameter("database");
                sessionContext.setDatabase(database);
            }
        }

        filterChain.doFilter(request, response);
    }

    @Override
    public void destroy()
    {
    }
}
