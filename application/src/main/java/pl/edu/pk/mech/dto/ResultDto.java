package pl.edu.pk.mech.dto;

public class ResultDto
{
    private final Result result;

    public ResultDto(Result result)
    {
        this.result = result;
    }

    public enum Result
    {
        SUCCESS,
        ERROR
    }

    public Result getResult()
    {
        return result;
    }
}
