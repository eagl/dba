package pl.edu.pk.mech.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import pl.edu.pk.mech.UserSessionContext;
import pl.edu.pk.mech.domain.Role;

import java.util.Iterator;

@Component
public class SessionUtil
{
    @Autowired
    private UserSessionContext sessionContext;

    public String getUsername()
    {
        SecurityContext securityContext = SecurityContextHolder.getContext();
        if (securityContext.getAuthentication() instanceof UsernamePasswordAuthenticationToken)
        {
            UsernamePasswordAuthenticationToken authentication = (UsernamePasswordAuthenticationToken) securityContext.getAuthentication();
            return authentication.getName();
        }
        return "";
    }

    public Role getRole()
    {
        SecurityContext securityContext = SecurityContextHolder.getContext();
        if (securityContext.getAuthentication() instanceof UsernamePasswordAuthenticationToken)
        {
            UsernamePasswordAuthenticationToken authentication = (UsernamePasswordAuthenticationToken) securityContext.getAuthentication();
            Iterator<GrantedAuthority> iterator = authentication.getAuthorities().iterator();
            if (iterator.hasNext())
            {
                GrantedAuthority authority = iterator.next();
                if (authority instanceof Role)
                {
                    return (Role) authority;
                }
            }
        }
        return Role.USER;
    }

    public String getSelectedDatabase()
    {
        return sessionContext.getDatabase();
    }

    public void setDatabase(String database)
    {
        sessionContext.setDatabase(database);
    }
}
