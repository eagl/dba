package pl.edu.pk.mech.dto;

import pl.edu.pk.mech.domain.Query;

public class QueryDto
{
    private final String hash;
    private final String description;
    private final Query.Type type;

    public QueryDto(String hash, String description, Query.Type type)
    {
        this.hash = hash;
        this.description = description;
        this.type = type;
    }

    public String getHash()
    {
        return hash;
    }

    public String getDescription()
    {
        return description;
    }

    public Query.Type getType()
    {
        return type;
    }
}
