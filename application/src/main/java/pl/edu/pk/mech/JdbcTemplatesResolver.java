package pl.edu.pk.mech;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import pl.edu.pk.mech.domain.Role;

@Component
public class JdbcTemplatesResolver
{
    @Autowired
    private ApplicationCache applicationCache;

    public JdbcTemplate resolve(String databaseName, Role role)
    {
        JdbcTemplatesWrapper wrapper = applicationCache.getDatabaseToJdbcTemplatesWrapperMap().get(databaseName);
        if (Role.ADMIN == role)
        {
            return wrapper.getJdbcTemplateForAdmin();
        }
        else
        {
            return  wrapper.getJdbcTemplateForUser();
        }
    }
}
