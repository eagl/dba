package pl.edu.pk.mech.util;

import org.springframework.stereotype.Component;
import pl.edu.pk.mech.domain.Query;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class QueryUtil
{
    public List<Query> filterByDisplayOnDashboard(List<Query> queryList)
    {
        return queryList.stream()
                .filter(query -> query.getConfiguration().isDisplayOnDashboard())
                .collect(Collectors.toList());
    }

    public List<Query> filterByDisplayInNav(List<Query> queryList)
    {
        return queryList.stream()
                .filter(query -> query.getConfiguration().isDisplayInNav())
                .collect(Collectors.toList());
    }
}
