package pl.edu.pk.mech.domain;


import org.springframework.security.core.GrantedAuthority;

public enum Role implements GrantedAuthority
{
    USER(0),
    ADMIN(1);

    private int bit;

    Role(int bit)
    {
        this.bit = bit;
    }

    @Override
    public String getAuthority()
    {
        return toString();
    }
}