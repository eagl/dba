package pl.edu.pk.mech.domain;

public class Configuration
{
    private boolean displayOnDashboard;
    private boolean displayInNav;
    private String label;

    public boolean isDisplayOnDashboard() {
        return displayOnDashboard;
    }

    public void setDisplayOnDashboard(boolean displayOnDashboard) {
        this.displayOnDashboard = displayOnDashboard;
    }

    public boolean isDisplayInNav()
    {
        return displayInNav;
    }

    public void setDisplayInNav(boolean displayInNav)
    {
        this.displayInNav = displayInNav;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }
}
