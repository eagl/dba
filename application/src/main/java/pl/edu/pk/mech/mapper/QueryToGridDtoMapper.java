package pl.edu.pk.mech.mapper;

import org.springframework.stereotype.Component;
import pl.edu.pk.mech.dto.IResultDto;
import pl.edu.pk.mech.dto.QueryGridDto;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@Component(value = "queryToGridDtoMapper")
public class QueryToGridDtoMapper implements Mapper<List<Map<String, Object>>, IResultDto>
{
    @Override
    public IResultDto map(List<Map<String, Object>> list)
    {
        QueryGridDto result = new QueryGridDto();

        Optional<Map<String, Object>> mapOptional = list.stream().findAny();
        mapOptional.ifPresent(firstRow->result.getLabels().addAll(firstRow.keySet()));
        list.forEach((oneRow)->result.getData().add(oneRow.values()));

        return result;
    }
}
