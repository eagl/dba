package pl.edu.pk.mech.mapper;

import org.springframework.stereotype.Component;
import pl.edu.pk.mech.domain.Query;
import pl.edu.pk.mech.dto.QueryDto;

import java.util.List;
import java.util.stream.Collectors;

@Component(value = "queryToQueryDtoMapper")
public class QueryToQueryDtoMapper implements Mapper<List<Query>,List<QueryDto>>
{
    @Override
    public List<QueryDto> map(List<Query> list)
    {
        return list.stream()
                .map(query -> new QueryDto(query.getHash(), query.getDescription(), query.getType()))
                .collect(Collectors.toList());
    }
}
