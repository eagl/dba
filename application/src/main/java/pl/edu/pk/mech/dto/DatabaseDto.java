package pl.edu.pk.mech.dto;

public class DatabaseDto
{
    private String name;
    private boolean online;

    public DatabaseDto()
    {
    }

    public DatabaseDto(String name)
    {
        this.name = name;
    }

    public DatabaseDto(String name, boolean online)
    {
        this.name = name;
        this.online = online;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public boolean isOnline()
    {
        return online;
    }

    public void setOnline(boolean online)
    {
        this.online = online;
    }
}
