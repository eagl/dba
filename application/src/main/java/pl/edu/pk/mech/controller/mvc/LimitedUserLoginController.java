package pl.edu.pk.mech.controller.mvc;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.edu.pk.mech.domain.Role;

import java.util.Arrays;

@Deprecated
//@Controller
public class LimitedUserLoginController
{
    @RequestMapping("/login-limited-user")
    public String loginLimitedUser()
    {
        SecurityContext securityContext = SecurityContextHolder.getContext();

        if (securityContext.getAuthentication().getPrincipal().equals("anonymousUser"))
        {
            securityContext.setAuthentication(buildDefaultAuthenticationToken());
        }

        return "redirect:/";
    }

    private UsernamePasswordAuthenticationToken buildDefaultAuthenticationToken()
    {
        // TODO [MM] Load users from application.properties
        return new UsernamePasswordAuthenticationToken("user", "user", Arrays.asList(Role.USER));
    }
}
