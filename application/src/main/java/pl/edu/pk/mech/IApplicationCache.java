package pl.edu.pk.mech;


public interface IApplicationCache {
    public Object getParameter(String name);
    public void addParameter(String key, Object value);
}
