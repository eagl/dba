package pl.edu.pk.mech.controller.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import pl.edu.pk.mech.JdbcTemplatesResolver;
import pl.edu.pk.mech.domain.Query;
import pl.edu.pk.mech.domain.Role;
import pl.edu.pk.mech.dto.IResultDto;
import pl.edu.pk.mech.dto.QueryEmptyDto;
import pl.edu.pk.mech.mapper.MethodFactoryMapper;
import pl.edu.pk.mech.repository.QueryRepository;
import pl.edu.pk.mech.util.SessionUtil;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Controller
public class QueryController extends RestController
{
    @Autowired
    private QueryRepository queryRepository;

    @Autowired
    private JdbcTemplatesResolver jdbcTemplatesResolver;

    @Autowired
    private SessionUtil sessionUtil;

    @Resource(name = "methodFactoryMapper")
    private MethodFactoryMapper methodFactoryMapper;

    @ResponseBody
    @RequestMapping(value = "/query/{hash}", method = RequestMethod.GET)
    public IResultDto executeQuery(@PathVariable String hash)
    {
        Role role = sessionUtil.getRole();
        String databaseName = sessionUtil.getSelectedDatabase();
        Query query = queryRepository.getQueryByHash(hash);

        if (query.getRoles().contains(role))
        {
            JdbcTemplate jdbcTemplate = jdbcTemplatesResolver.resolve(databaseName, role);
            String sql = query.getSql();
            List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql);
            return methodFactoryMapper.mapResult(query.getType(), rows);
        }

        return new QueryEmptyDto();
    }

    @ResponseBody
    @RequestMapping(value = "/query/{name}/{database}", method = RequestMethod.GET)
    public IResultDto executeQueryByName(@PathVariable String name, @PathVariable String database)
    {
        Role role = sessionUtil.getRole();
        Query query = queryRepository.getQueryByName(name);

        if (query.getRoles().contains(role))
        {
            JdbcTemplate jdbcTemplate = jdbcTemplatesResolver.resolve(database, role);
            String sql = query.getSql();
            List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql);
            return methodFactoryMapper.mapResult(query.getType(), rows);
        }

        return new QueryEmptyDto();
    }
}
