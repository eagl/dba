package pl.edu.pk.mech.repository;

import pl.edu.pk.mech.domain.Query;
import pl.edu.pk.mech.domain.Role;

import java.util.List;

public interface QueryRepository
{
    List<Query> getQueryListByRole(Role role);
    Query getQueryByHash(String hash);
    Query getQueryByName(String name);
}
