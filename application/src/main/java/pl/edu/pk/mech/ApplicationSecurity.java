package pl.edu.pk.mech;

import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import pl.edu.pk.mech.domain.Role;

import java.util.Arrays;

@Order(SecurityProperties.ACCESS_OVERRIDE_ORDER)
public class ApplicationSecurity extends WebSecurityConfigurerAdapter
{
    @Override
    protected void configure(HttpSecurity http) throws Exception
    {
        http.csrf().disable();

        http.authorizeRequests()
            .antMatchers("/", "/static/**", "/rest/**").permitAll()
            .anyRequest().authenticated();

        http.formLogin()
            .loginPage("/").failureUrl("/?error").permitAll()
            .and()
            .logout().permitAll();
    }

    @Override
    public void configure(AuthenticationManagerBuilder auth) throws Exception
    {
        // TODO [MM] Load users from application.properties
        auth.inMemoryAuthentication()
            .withUser("admin").password("admin").authorities(Arrays.asList(Role.ADMIN))
            .and()
            .withUser("user").password("user").authorities(Arrays.asList(Role.USER));
    }

    @Override
    public void configure(WebSecurity security)
    {
        security.ignoring().antMatchers("/css/**","/fonts/**","/font-awesome-4.1.0/**");
    }
}
