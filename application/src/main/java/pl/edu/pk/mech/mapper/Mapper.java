package pl.edu.pk.mech.mapper;

public interface Mapper <IN,OUT>
{
    OUT map(IN list);
}
