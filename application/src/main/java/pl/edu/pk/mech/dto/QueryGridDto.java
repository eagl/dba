package pl.edu.pk.mech.dto;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

public class QueryGridDto extends QueryResponse
{
    private List<String> labels = new LinkedList<>();
    private List<Collection<Object>> data = new LinkedList<>();

    public List<String> getLabels()
    {
        return labels;
    }

    public void setLabels(List<String> labels)
    {
        this.labels = labels;
    }

    public List<Collection<Object>> getData() {
        return data;
    }

    public void setData(List<Collection<Object>> data) {
        this.data = data;
    }
}
