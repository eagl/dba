package pl.edu.pk.mech.controller.mvc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.edu.pk.mech.ApplicationCache;
import pl.edu.pk.mech.domain.Role;
import pl.edu.pk.mech.util.SessionUtil;
import java.util.Arrays;
import java.util.Map;

@Controller
public class HomeController
{
    @Autowired
    private SessionUtil sessionUtil;

    @Autowired
    private ApplicationCache applicationCache;

    @RequestMapping("/")
    public String home(Map<String, Object> model)
    {
        resolveDefaultDatabase();
        resolveDefaultUser();

        model.put("selectedDatabase", sessionUtil.getSelectedDatabase());
        model.put("username", sessionUtil.getUsername());

        return "index";
    }

    private void resolveDefaultDatabase()
    {
        if (sessionUtil.getSelectedDatabase() == null)
        {
            String defaultDatabase = applicationCache.getDatabaseToJdbcTemplatesWrapperMap().keySet().iterator().next();
            sessionUtil.setDatabase(defaultDatabase);
        }
    }

    private void resolveDefaultUser()
    {
        SecurityContext securityContext = SecurityContextHolder.getContext();
        if (securityContext.getAuthentication().getPrincipal().equals("anonymousUser"))
        {
            securityContext.setAuthentication(buildDefaultAuthenticationToken());
        }
    }

    private UsernamePasswordAuthenticationToken buildDefaultAuthenticationToken()
    {
        // TODO [MM] Load users from application.properties
        return new UsernamePasswordAuthenticationToken("user", "user", Arrays.asList(Role.USER));
    }
}
