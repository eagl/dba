package pl.edu.pk.mech;

import org.springframework.jdbc.core.JdbcTemplate;

public class JdbcTemplatesWrapper
{
    private final JdbcTemplate jdbcTemplateForAdmin;
    private final JdbcTemplate jdbcTemplateForUser;

    public JdbcTemplatesWrapper(JdbcTemplate jdbcTemplateForAdmin, JdbcTemplate jdbcTemplateForUser)
    {
        this.jdbcTemplateForAdmin = jdbcTemplateForAdmin;
        this.jdbcTemplateForUser = jdbcTemplateForUser;
    }

    public JdbcTemplate getJdbcTemplateForAdmin()
    {
        return jdbcTemplateForAdmin;
    }

    public JdbcTemplate getJdbcTemplateForUser()
    {
        return jdbcTemplateForUser;
    }
}
