package pl.edu.pk.mech.controller.rest;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Controller
public class DbTestController extends RestController
{
    private static final String SQL_SELECT = "SELECT * FROM AQ$_INTERNET_AGENT_PRIVS";
    private JdbcTemplate jdbcTemplate;

    @RequestMapping(value = "/db", method = RequestMethod.GET)
    public @ResponseBody List<String> db()
    {
        List<String> results = new ArrayList<>();

        List<Map<String, Object>> rows = jdbcTemplate.queryForList(SQL_SELECT);
        for (Map row : rows)
        {
            String name = (String) row.get("AGENT_NAME");
            results.add(name);
        }

        return results;
    }

    @Resource(name = "oracle.jdbcTemplate1.admin")
    public void setJdbcTemplate(JdbcTemplate jdbcTemplate)
    {
        this.jdbcTemplate = jdbcTemplate;
    }
}