String.prototype.hashCode = function() {
    var hash = 0, i, chr, len;
    if (this.length == 0) return hash;
    for (i = 0, len = this.length; i < len; i++) {
        chr   = this.charCodeAt(i);
        hash  = ((hash << 5) - hash) + chr;
        hash |= 0; // Convert to 32bit integer
    }
    if (hash < 0) {
        hash *= -1;
    }
    return hash;
};

var dba = {};

(function(namespace)
{
    function Application()
    {
        var that = this;

        that.run = function(logged)
        {
            namespace.logged = logged;
            invokeInitializers();
        }

        function invokeInitializers()
        {
            for (var i = 0, max = namespace.initializers.length; i < max; i++)
            {
                namespace.initializers[i].initialize();
            }
        };
    };

    namespace.Application = Application;
    namespace.logged = false;
    namespace.CONTAINERS_WRAPPER_ID = '#container-wrapper';
    namespace.ONE_VALUE_COMPONENTS_CONTAINER_ID = '#one-value-components-container';
    namespace.PLOTS_CONTAINER_ID = '#plots-container';
    namespace.GRIDS_CONTAINER_ID = '#grids-container';
    namespace.STATUS_CONTAINER_ID = '#status-container';
    namespace.components = [];

    namespace.isLogged = function () {
        return namespace.logged;
    };
    namespace.isNotLogged = function () {
        return !namespace.logged;
    };
}(dba));

(function(namespace)
{
    function DatabasesStatusInitializer()
    {
        var that = this;

        that.initialize = function()
        {
            if (namespace.isLogged()) return;

            $.get('rest/databases-status', function(data) {
                for (var i = 0, max = data.length; i < max; i++) {
                    var context = {
                        database: data[i].name,
                        online: data[i].online
                    };
                    var component = namespace.componentFactory.create(namespace.ComponentType.STATUS, context);
                    component.render();

                    if (component.online) {
                        var containerId = '#' + component.hash + ' .left-plot-column';
                        renderPlot('STUDENT_DATA', component.database, containerId);

                        containerId = '#' + component.hash + ' .right-plot-column';
                        renderPlot('STUDENT_INDEX', component.database, containerId);
                    }
                }
            });

            function renderPlot(query, database, containerId)
            {
                $.get(encodeURI('rest/query/' + query + '/' + database), function(restData) {
                    var salt = query + database;
                    var context = {
                        hash: salt.hashCode(),
                        description: query
                    };
                    var component = namespace.componentFactory.create(namespace.ComponentType.PLOT, context);
                    component.renderWithData(containerId, restData);
                });
            };
        };
    }

    function DashboardInitializer()
    {
        var that = this;

        that.initialize = function()
        {
            if (namespace.isNotLogged()) {
                $(namespace.CONTAINERS_WRAPPER_ID).hide();
                return;
            }

            $.get('rest/dashboard-query-list', function(queryList) {
                for (var i = 0, max = queryList.length; i < max; i++) {
                    var query = queryList[i];
                    var context = {
                        hash: query.hash,
                        description: query.description
                    };
                    var component = namespace.componentFactory.create(query.type, context);
                    component.render();
                }
            });
        };
    };

    function NavigationInitializer()
    {
        var that = this;
        that.NAV_CONTAINER_ID = "#side-menu";
        that.NAV_LINK_TEMPLATE_ID = "#nav-link-template";

        that.initialize = function()
        {
            if (namespace.isNotLogged()) return;

            $.get('rest/navigation-query-list', function(queryList) {
                for (var i = 0, max = queryList.length; i < max; i++)
                {
                    var query = queryList[i];
                    var context = {
                        hash: query.hash,
                        description: query.description
                    };
                    var component = namespace.componentFactory.create(query.type, context);
                    if (namespace.componentUtil.notContains(component))
                    {
                        namespace.components.push(component);
                    }
                }

                renderNavigation();
                attachOnClickEvent();

                // moved from sb-admin.js file
                $(that.NAV_CONTAINER_ID).metisMenu();
            });
        };

        function renderNavigation()
        {
            for (var i = 0, max = namespace.components.length; i < max; i++)
            {
                var component = namespace.components[i];
                var data = {
                    hash: component.hash,
                    description: component.description,
                    css: component.state === namespace.ComponentState.RENDERED ? "active" : ""
                };
                $(that.NAV_LINK_TEMPLATE_ID).tmpl(data).appendTo(that.NAV_CONTAINER_ID);
            }
        }

        function attachOnClickEvent()
        {
            $(that.NAV_CONTAINER_ID + " li a").on("click", function(event) {
                event.preventDefault();
                var link = $(this);
                var hash = link.data('hash');
                var component = namespace.componentUtil.findByHash(hash.toString());

                if (component.state === namespace.ComponentState.NEW)
                {
                    component.render();
                }
                else if (component.state === namespace.ComponentState.RENDERED)
                {
                    component.node.hide();
                    component.state = namespace.ComponentState.HIDDEN;
                }
                else if (component.state === namespace.ComponentState.HIDDEN)
                {
                    component.node.show();
                    component.state = namespace.ComponentState.RENDERED;
                }
            });
        }
    };

    function DatabasesInitializer()
    {
        var that = this;
        that.DATABASES_CONTAINER_ID = "#available-databases";
        that.DATABASE_TEMPLATE_ID = "#database-template";

        that.initialize = function()
        {
            if (namespace.isNotLogged()) return;

            $.get( "/rest/available-databases", function(data) {
                $(that.DATABASE_TEMPLATE_ID).tmpl(data).appendTo(that.DATABASES_CONTAINER_ID);

                $(that.DATABASES_CONTAINER_ID + ' li a').click(function(event) {
                    var database = $(this).data('name');
                    var data = JSON.stringify({ name: database });

                    $.ajax({
                        type: "POST",
                        url: "/rest/database",
                        timeout: 30000,
                        data: data,
                        contentType: "application/json",
                        dataType: "json",
                        success: function(response) {
                            window.location.reload();
                        }
                    });
                });
            });
        }
    }

    namespace.initializers = [];
    namespace.initializers.push(new DatabasesStatusInitializer());
    namespace.initializers.push(new DashboardInitializer());
    namespace.initializers.push(new NavigationInitializer());
    namespace.initializers.push(new DatabasesInitializer());
}(dba));

(function(namespace)
{
    function OneValueComponent(hash, description)
    {
        var that = this;
        that.TEMPLATE_ID = "#one-value-template";
        that.state = namespace.ComponentState.NEW;
        that.type = namespace.ComponentType.ONE_VALUE;
        that.node;

        that.hash = hash;
        that.description = description;

        that.render = function()
        {
            $.get('rest/query/' + that.hash, function(restData) {
                var valueTemp = restData.data.value;
                if (restData.data.value.charAt(0) === ',' ) {
                    console.log('DATA IS TO SMALL');
                    valueTemp = '0.'+restData.data.value.substring(1, restData.data.value.length);
                }

                var data = {
                    hash: that.hash,
                    description: that.description,
                    data: valueTemp
                };

                $(that.TEMPLATE_ID).tmpl(data).appendTo(namespace.ONE_VALUE_COMPONENTS_CONTAINER_ID);
                that.node = $('#' + that.hash);
                that.state = namespace.ComponentState.RENDERED;
            });
        };
    };

    function PlotComponent(hash, description)
    {
        var that = this;
        that.TEMPLATE_ID = "#plot-template";
        that.state = namespace.ComponentState.NEW;
        that.type = namespace.ComponentType.PLOT;
        that.node;
        that.plotNode;
        that.plotSettings = {
            series: {
                pie: {
                    show: true
                }
            },
            grid: {
                hoverable: true
            },
            tooltip: true,
            tooltipOpts: {
                content: "%p.0%, %s",
                shifts: {
                    x: 20,
                    y: 0
                },
                defaultTheme: false
            }
        };

        that.hash = hash;
        that.description = description;

        that.render = function()
        {
            var data = {
                hash: that.hash,
                description: that.description,
                more_details: '',
                columns: 6
            };

            $(that.TEMPLATE_ID).tmpl(data).appendTo(namespace.PLOTS_CONTAINER_ID);
            that.node = $('#' + that.hash);
            that.state = namespace.ComponentState.RENDERED;
            $.get('rest/query/' + that.hash, function(restData) {
                renderPlot(restData);
            });
        };

        that.renderWithData = function(containerId, restData) {
            var data = {
                hash: that.hash,
                description: that.description,
                more_details: '',
                columns: 12
            }

            $(that.TEMPLATE_ID).tmpl(data).appendTo(containerId);
            that.node = $('#' + that.hash);
            that.state = namespace.ComponentState.RENDERED;
            renderPlot(restData);
        };

        function renderPlot(restData)
        {
            var data = [];
            for(var property in restData.data){
                var tempValue = restData.data[property];
                var tempProperty = property+" ["+tempValue+" MB]";
                data.push({label: tempProperty, data: tempValue});
            }

            if(restData.description != null) {
                var details = $('#'+that.hash).find('.more-details');
                details.html(restData.description);
                details.show();
            }

            var maxUsage = $('#'+that.hash).find('.max-usage');
            maxUsage.text(restData.max);

            that.plotNode = $('#plot-' + that.hash);
            $.plot(that.plotNode, data, that.plotSettings);
        };
    };

    function GridComponent(hash, description)
    {
        var that = this;
        that.TEMPLATE_ID = "#grid-template";
        that.state = namespace.ComponentState.NEW;
        that.type = namespace.ComponentType.GRID;
        that.node;
        that.gridNode;
        that.gridSettings =  {
            "language": {
                "paginate": {
                    "next": "Następna strona",
                    "previous": "Poprzednia strona"
                },
                "info": "Strona _PAGE_ z _PAGES_",
                "search": "Filtruj:",
                "infoEmpty": "Brak danych",
                "emptyTable": "---",
                "lengthMenu": "_MENU_ wyników na stronę",
                "infoFiltered": "(wyszukano z _MAX_ wyników)"
            }
        }

        that.hash = hash;
        that.description = description;

        that.render = function()
        {
            $.get('rest/query/' + that.hash, function(restData) {
                var data = {
                    hash: that.hash,
                    description: that.description,
                    data: restData
                };
                $(that.TEMPLATE_ID).tmpl(data).appendTo(namespace.GRIDS_CONTAINER_ID);
                that.node = $('#' + that.hash);
                that.gridNode = $('#grid-' + that.hash)
                that.gridNode.dataTable(that.gridSettings);
                that.state = namespace.ComponentState.RENDERED;
            });
        };
    };

    function StatusComponent(database, online)
    {
        var that = this;
        that.TEMPLATE_ID = "#status-template";
        that.type = namespace.ComponentType.STATUS;
        that.node;

        that.database = database;
        that.online = online;
        that.hash = database.hashCode();

        that.render = function()
        {
            var data = {
                database: that.database,
                hash: that.hash,
                cssClass: that.online ? "btn-success" : "btn-danger",
                status: that.online ? "ON" : "OFF"
            };
            $(that.TEMPLATE_ID).tmpl(data).appendTo(namespace.STATUS_CONTAINER_ID);
        };
    };

    function ComponentFactory()
    {
        var that = this;

        that.create = function(componentType, context)
        {
            console.log('ComponentFactory::create(' + componentType + ')');
            switch (componentType)
            {
                case namespace.ComponentType.ONE_VALUE:
                    return new OneValueComponent(context.hash, context.description);
                case namespace.ComponentType.PLOT:
                    return new PlotComponent(context.hash, context.description);
                case namespace.ComponentType.GRID:
                    return new GridComponent(context.hash, context.description);
                case namespace.ComponentType.STATUS:
                    return new StatusComponent(context.database, context.online);
            }

        };
    };

    namespace.componentFactory = new ComponentFactory();
    namespace.ComponentType = {
        ONE_VALUE: "ONE_VALUE",
        PLOT: "PLOT",
        GRID: "GRID",
        STATUS: "STATUS"
    };
    namespace.ComponentState = {
        NEW: "NEW",
        RENDERED: "RENDERED",
        HIDDEN: "HIDDEN"
    }
    namespace.model = {};
    namespace.model.OneValueComponent = OneValueComponent;
    namespace.model.PlotComponent = PlotComponent;
    namespace.model.GridComponent = GridComponent;
    namespace.model.StatusComponent = StatusComponent;
}(dba));

(function(namespace)
{
    function ComponentUtil()
    {
        var that = this;

        that.contains = function(component)
        {
            for (var i = 0, max = namespace.components.length; i < max; i++)
            {
                if (component.hash === namespace.components[i].hash)
                {
                    return true;
                }
            }

            return false;
        };

        that.notContains = function(component)
        {
            return !that.contains(component);
        };

        that.findByHash = function(hash)
        {
            for (var i = 0, max = namespace.components.length; i < max; i++)
            {
                if (namespace.components[i].hash === hash)
                {
                    return namespace.components[i];
                }
            }
        };
    }

    namespace.componentUtil =  new ComponentUtil();
}(dba));
