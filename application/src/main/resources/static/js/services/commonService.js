function getListAvailableServersToChose() {
    $.get( "/rest/servers", function(data) {
        $("#serverTemplate").tmpl(data).appendTo("#serversToChose");

        var databaseList = $("input[type=radio]");
        databaseList.click(function(event) {
            $("#databaseHidden").val($(this).val());
        });

        var firstDataase = databaseList.first();
        firstDataase.attr("checked", "checked");
        $("#databaseHidden").val(firstDataase.val());
    });
}

function getQueryForLeftNav() {
    $.get( "/rest/queriesLeftNav", function(data) {
        if(data.length === 0){
            $("#administratorHeader").hide();
            $("#infoToExecuteQuery").hide();
            $("#warrningToLoginAdmin").show();
        }else{
            $("#leftNavQueries").tmpl(data).appendTo(".leftNavQueries");
        }
    });
}

function getQueryDefault() {
    $.get( "rest/queriesDefault", function(data) {
        $("#queriesDefaultTemplate").tmpl(data).appendTo("#queriesDefault");
    });
}

function establishDashBoardEvents() {
    leftNavQueriesOnClick();
}

function leftNavQueriesOnClick() {
    $( ".leftNavQueries" ).on("click","li.clickable", function(event) {
        var idQuery = $('a', this).attr('value');
        //TODO: jesli jest juz taki wykres to nie rob nic

        //TODO: trzeba zawolac rest, ma liste etykiet i wyniki i rodzaj wykresu jaki ma zostac narysowany
        var data = [{label: "Series 0", data: 5}, {label: "Series 1", data: 5}, {label: "Series 2", data: 10}, {label: "Series 3", data: 20}];
        var emptyData = [{description: "To jest zajebiscie magiczny select", idQuery: idQuery}];

        $("#pieFlotChart").tmpl(emptyData).insertAfter("#administratorHeader");
        fillPieChart(data, idQuery);
    });
}

function fillPieChart(data, idQuery) {
    var plotObj = $.plot($("#flot-pie-chart-"+idQuery), data, {
        series: {
            pie: {
                show: true
            }
        },
        grid: {
            hoverable: true
        },
        tooltip: true,
        tooltipOpts: {
            content: "%p.0%, %s", // show percentages, rounding to 2 decimal places
            shifts: {
                x: 20,
                y: 0
            },
            defaultTheme: false
        }
    });
}